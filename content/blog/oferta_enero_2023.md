---
title: 'Oferta de trabajo enero 2023'
date: 2022-12-19T17:01:34+07:00
draft: true
---

# Desarrolladora software en librecoop

## ¿Quiénes somos?

Librecoop es un colectivo de software libre para el cambio social, que quiere colaborar con colectivos autogestionados y entidades de la economía social y solidaria (ESS) creando, adaptando y manteniendo soluciones de código abierto. Queremos ampliar nuestro equipo, aumentando también la pluralidad y diversidad de nuestra organización. Actualmente trabajamos mano a mano con Ecooo Energía Ciudadana que es una cooperativa cuyo objetivo es poner la energía en manos de las personas, y contribuir al cambio social y económico.

## ¿Qué buscamos?

Buscamos una persona que se dedique a la informática, que sea afín a los principios de la economía social y solidaria y de los movimientos sociales, que esté interesada por el software libre. Además, buscamos lo mismo que en todas las demás ofertas: gente creativa, sin miedo a los retos, con iniciativa, capaz de organizarse por su cuenta, con compromiso, talento, ganas de aprender, sentido del humor. También se valoran la experiencia y los idiomas.

## ¿Qué ofrecemos?

El objetivo último es que la persona que se incorpore pertenezca a Librecoop. Esto significa que además de estar empleada, participe activamente en la toma de decisiones de la cooperativa.

- La satisfacción de saber que tu trabajo tiene un impacto social.
- Posibilidad de trabajar con movimientos sociales y cooperativas de la ESS, y compañarlas en sus procesos de transformación digital.
- Aprendizaje constante.
- **Condiciones de entrada**: 1458€ brutos al mes por 25 horas semanales con jornada flexible siempre y cuando te coordines con compañeras y compañeros. Posibilidad de teletrabajo (en función de proyectos) Zona Lavapiés (Madrid).

**Incorporación**: a partir de finales de enero 2023 en Librecoop, formar parte de una cooperativa en la cual tu opinión será exactamente igual de importante que la de cualquier persona.

## ¿Qué vamos a hacer?

Inicialmente, las tareas se focalizarán en resolver las necesidades tecnológicas de una de las entidades de la ESS, Ecooo Energía Ciudadana. Trabajaremos con:

- Desarrollo web full stack
- Python, Odoo, Django
- PostgreSQL
- Nodejs, React, Angular
- GitLab CI, Docker
- Nginx/certbot - herramientas de webadmin en general
- Estrategias de backups y HADR
- Gestión de servidores Linux
- Nextcloud

No te preocupes si no las dominas todas, desde Librecoop entedemos que este es un campo en constante cambio y que es dificil estar encima de todos los cambios. Nos parece más importante encajar a nivel personal y político, la parte técnica siempre se puede aprender ☺️.

Y si manejas algo que no está en la lista -sí, falta Ruby, Strapi, Vue...- cuéntanos cómo podemos empezar a sacarle partido.
Si te interesa, escríbenos a librecoop@protonmail.com y cuéntanos por qué te gustaría trabajar con nosotras. No dudes en incluir cualquier cosa interesante: currículum, proyectos que hayas hecho tú, cuenta de gitlab/bitbucket/github, experiencias en movimientos sociales o de militancia o voluntariados, listados de notas, capturas de pantalla con puntuaciones estratosféricas en algún juego extremadamente difícil.

**Fecha límite para enviar CV: 15/01/2023**

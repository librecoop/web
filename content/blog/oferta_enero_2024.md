---
title: 'Oferta de trabajo enero 2024'
date: 2023-11-21T12:40:55+01:00
layout: blog
---

# Desarrolladora software en librecoop

## ¿Quiénes somos?

Librecoop es un colectivo de software libre para el cambio social, que quiere colaborar con colectivos autogestionados y entidades de la economía social y solidaria (ESS) creando, adaptando y manteniendo soluciones de código abierto. Queremos ampliar nuestro equipo, aumentando también la pluralidad y diversidad de nuestro colectivo. Actualmente trabajamos principalmente con Ecooo Energía Ciudadana, que es una cooperativa cuyo objetivo es poner la energía en manos de las personas, y estamos empezando a colaborar con otras entidades de la ESS.

## ¿Qué buscamos?

Buscamos una persona que se dedique a la informática, que sea afín a los principios de la economía social y solidaria y de los movimientos sociales, y que esté interesada por el software libre. Además, buscamos lo mismo que en todas las demás ofertas: gente creativa, sin miedo a los retos, con iniciativa, capaz de organizarse por su cuenta, con compromiso, talento, ganas de aprender, sentido del humor. También se valoran la experiencia y los idiomas.

## ¿Qué ofrecemos?

El objetivo último es que la persona que se incorpore pertenezca a Librecoop. Esto significa que además de estar empleada, participe activamente en la toma de decisiones de la cooperativa.

- La satisfacción de saber que tu trabajo tiene un impacto social
- Posibilidad de trabajar con movimientos sociales y cooperativas de la ESS, y compañarlas en sus procesos de transformación digital
- Aprendizaje constante
- Formar parte de una cooperativa en la cual tu opinión será exactamente igual de importante que la de cualquier persona

**Condiciones de entrada:** 1458€ brutos al mes por 25 horas semanales con jornada flexible siempre y cuando te coordines con compañeras y compañeros. La mayoría del tiempo solemos teletrabajar, pero también nos es importante coincidir en persona al menos 1 o 2 veces a la semana. Zona Lavapiés (Madrid)

**Incorporación:** a partir de inicio de enero 2024 en Librecoop

## ¿Qué vamos a hacer?

En el momento tenemos estas líneas de trabajo principales:

**Desarrollo de módulos de Odoo para Ecooo:**

Odoo es un ERP de software libre escrito en Python y modular. Programamos módulos customizados para cubrir las necesidades que tiene Ecooo para su funcionamiento interno como pueden ser el control de producciones de plantas fotovoltaicas o la gestión de los inversores y los repartos que les corresponden. [Link](https://gitlab.com/librecoop/ecooo/ecooo-odoo-addons/)

**Desarrollo de la plataforma [Upmeup](https://upmeup.es):**

Hemos desarrollado la app de búsqueda de empleo para la economía social y solidaria, escrita con Nest.js en el backend y Angular en el front, y usando GraphQL y MongoDB. [Link](https://gitlab.com/librecoop/up-me-up)

**Despliegue de servicios de internet autoalojados para uso propio y para otras entidades:**

Mantenemos servidores alquilados y usamos nuestro propio sistema de [IaC](https://es.wikipedia.org/wiki/Infraestructura_como_c%C3%B3digo) usando Ansible, Docker y Docker compose para desplegar servicios que usamos nosotres y las entidades a los que los ofrecemos, como pueden ser odoo, wordpress, almacenamiento online, gestor de contraseñas, servidor de correos... [Link](https://gitlab.com/librecoop/librecloud-ansible)

Si te interesa, escríbenos a <contacto@librecoop.es> y cuéntanos por qué te gustaría trabajar con nosotras. No dudes en incluir cualquier cosa interesante: currículum, proyectos que hayas hecho tú, cuenta de gitlab/bitbucket/github, experiencias en movimientos sociales o de militancia o voluntariados, listados de notas, capturas de pantalla con puntuaciones estratosféricas en algún juego extremadamente difícil.

_Fecha límite para enviar CV: **13**.**12**.2023_

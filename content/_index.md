---
title: 'Homepage'
meta_title: 'Librecoop'
description: "Cooperativa de software libre para la economía social."
intro_image: "images/illustrations/pair_programming.svg"
intro_image_absolute: true
intro_image_hide_on_mobile: true
---

# Librecoop - Software libre para el cambio social

Apoyamos a colectivos autogestionados y entidades de la Economía Social y Solidaria (ESS) creando, adaptando y manteniendo soluciones de código abierto.

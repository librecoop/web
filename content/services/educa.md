---
title: "Colaboración con entorno educativo"
date: 2022-19-16T11:14:54+10:00
featured: true
draft: false
weight: 5
---

Queremos ofrecer una alternativa al modelo de prácticas y TFG/TFM actual. Queremos que los estudiantes dediquen su tiempo en aportar valor a los movimientos sociales y a entidades de la economía social y solidaria. Colaborar con universidades y otros entornos educativos para desarrollar proyectos formativos pero útiles para la sociedad.
---
title: "Transición digital"
date: 2022-10-16T11:37:46+10:00
draft: false
featured: true
weight: 1
---

Ayudamos a cooperativas a mejorar sus procesos y transicionar hacia el software libre.

---
title: 'Sobre nosotrxs'
date: 2018-02-22T17:01:34+07:00
---

Librecoop es un colectivo de software libre para el cambio social, que quiere colaborar con colectivos autogestionados y entidades de la economía social y solidaria (ESS) creando, adaptando y manteniendo soluciones de código abierto. 

Creemos en la autogestión y en la horizontalidad como pricipios clave para el funcionamiento de cualquier grupo humano. Nos organizamos de forma asamblearia y tomamos decisiones por consenso.

Somos plenamente conscientes de los límites biofisicos del planeta y de la carga que la expansión del uso de la tecnología pone sobre estos. Por eso ponemos empeño en desarrollar soluciones eficientes, que consuman el menor número de recursos posibles y en utilizar infrastructuras que se ajusten a estos mismos criterios.